# File compressor
## Introduction
Compress and uncompress a TXT file, with Haussman algorithm. You can find some examples of compressed, uncompressed and dictionnary files in data folder.

## How can I use it ?
Compile the .cpp file, and execute it (or use directly the Linux executable) with following parameters :
* First parameter : Type of the action, between compress / uncompress / dictionnary / all .
  * compress : compress the TXT file to a BRYNANUM file
  * uncompress : uncompress the BRYNANUM file to a TXT file
  * dictionnary : give the file translation between the TXT and BRYNANUM files
  * all : realize the three previous actions
* Second parameter : The path of the TXT file.

If the file exists, it is overwritten.

## Documentation
You can find all technical documentation in the "doc" folder, created with Doxygen. Open doc/index.html in a browser from your computer.
You are able to see the global structuration of variable in this C++ file :

![Global structure of variable](https://gitlab.com/Brynanum/file-compressor/raw/master/Structure.png)

These element (noticely the tree and the dictionnary) are realized in the following functions respectively :
* treeGenerate() realize the frequency of each character ;
* buildTree() construct leaf nodes, then internal nodes until root ;
* generateCodes() build a dictionnary according to the previous tree.

When the algorithm did these steps, it looks for the asked action : compress it, uncompress it, extract the dictionnary or all of them. Each action has his function to execute : brynanumCompress, brynanumUncompress, brynanumDictionnaryExtract.

To conclude, the main() function controls parameters, and execute the given action(s).

## Required C++ libraries
The C++ libraries are listed at the beginning of the CPP file. These are :
* iostream
* fstream
* cstring
* iterator
* algorithm
* queue
* map
* climits

## License
[GNU Free Documentation License 1.2](http://www.gnu.org/licenses/fdl-1.2.html)

## Credits
The CPP file inspired an algorithm on [Rosetta Code](https://rosettacode.org/wiki/Huffman_coding#C.2B.2B).

# Contributor
Bryan Fauquembergue

# Improvement
This project will not have some amelioration. However, you can fork it and share your own ameliorations, while you expect the license.
