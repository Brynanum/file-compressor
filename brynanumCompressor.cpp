/**
 * @file brynanumCompressor.cpp
 * @brief Source code of the file compression, using Haussman algorithm
 * @author Bryan Fauquembergue
 * You can find more information about this file on https://gitlab.com/Brynanum/file-compressor .
 */

#include <iostream>
#include <fstream>
#include <cstring>
#include <iterator>
#include <algorithm>
#include <queue>
#include <map>
#include <climits>

#define FILE_SEPARATION "|" // Separation between character and binary value in the dictionnary of translations

typedef std::vector<bool> HuffCode;
typedef std::map<char, HuffCode> HuffCodeMap;

const int UniqueSymbols = 1 << CHAR_BIT;
std::string filePath;
HuffCodeMap tree;

/**
 * @class INode
 * @brief Class that contains the number of INode found
 * This value will be a part of the binary value in LeafNode.
 */
class INode {
	public:
		const int f;
	 
		virtual ~INode() {}
	 
	protected:
		INode(int f) : f(f) {}
};

/**
 * @class InternalNode
 * @brief Class that give links between INode
 * You can fin here the left and right children in the tree.
 */
class InternalNode : public INode {
	public:
		INode *const left;
		INode *const right;
	 
		InternalNode(INode* c0, INode* c1) : INode(c0->f + c1->f), left(c0), right(c1) {}
		~InternalNode() {
		    delete left;
		    delete right;
		}
};

/**
 * @class LeafNode
 * @brief INode that contain the representative letter
 * This class give associate the letter at its value. Indeed, each following InternalNode gives a part of the binary value.
 */
class LeafNode : public INode {
	public:
		const char c;
	 
		LeafNode(int f, char c) : INode(f), c(c) {}
};

/**
 * @struct NodeCmp
 * @brief Transform the number of INode found in a binary value
 * This binary value is the representative form of the count, associated at its character by LeafNode class
 */
struct NodeCmp {
    bool operator()(const INode* lhs, const INode* rhs) const { return lhs->f > rhs->f; }
};

/**
 * @fn BuildTree
 * @brief Construct the tree according to the frequencies of characters
 * @param frequencies The array that give each character and its existing number
 * @return INode* The entire built tree (by its root)
 * Notice : a tree can be represented by its root. Indeed, the root has two children, and each one can represent a tree ; recursively until leaves.
 */
INode* BuildTree(const int (&frequencies)[UniqueSymbols]) {
    std::priority_queue<INode*, std::vector<INode*>, NodeCmp> trees;
 
    for (int i = 0; i < UniqueSymbols; ++i) {
        if(frequencies[i] != 0)
            trees.push(new LeafNode(frequencies[i], (char)i));
    }
    while (trees.size() > 1) {
        INode* childL = trees.top();
        trees.pop();
 
        INode* childR = trees.top();
        trees.pop();
 
        INode* parent = new InternalNode(childL, childR);
        trees.push(parent);
    }
    return trees.top();
}

/**
 * @fn GenerateCodes
 * @brief Realize the dictionnary character <--> binary value, according to the built tree
 * @param node The built tree (by its root)
 * @param prefix A memory variable of a part of binary values, when the letter can't be found in this recursive function (when a leaf isn't reached)
 * @param outCodes The dictionnary character <--> binary
 * Notice : a tree can be represented by its root. Indeed, the root has two children, and each one can represent a tree ; recursively until leaves.
 */
void GenerateCodes(const INode* node, const HuffCode& prefix, HuffCodeMap& outCodes) {
    if (const LeafNode* lf = dynamic_cast<const LeafNode*>(node)) {
        outCodes[lf->c] = prefix;
    } else if (const InternalNode* in = dynamic_cast<const InternalNode*>(node)) {
        HuffCode leftPrefix = prefix;
        leftPrefix.push_back(false);
        GenerateCodes(in->left, leftPrefix, outCodes);
 
        HuffCode rightPrefix = prefix;
        rightPrefix.push_back(true);
        GenerateCodes(in->right, rightPrefix, outCodes);
    }
}

/**
 * @fn treeGenerate
 * @brief Construct the frequencies of the characters in the given TXT file, then construct the tree and the dictionnary
 * @param filePath The path of the uncompressed file
 * @return HuffCodeMap The dictionnary of translations character <--> binary value
 * The tree is saved in a global value.
 */
HuffCodeMap treeGenerate(std::string filePath) {
	// Generate frequencies of the characters
    int frequencies[UniqueSymbols] = {0};
	std::ifstream file(filePath);
	std::string line, text;
	while(getline(file,line)) {
		text+=line;
	}	
	file.close();
	const char* ptr;
	ptr=text.c_str();
    while (*ptr != '\0')
        ++frequencies[*ptr++];

	// Construct the tree
    INode* root = BuildTree(frequencies);
 
	// Construct the dictionnary
    HuffCodeMap codes;
    GenerateCodes(root, HuffCode(), codes);
    delete root;

    return codes;
}

/**
 * @fn brynanumDictionnaryExtract
 * @brief Give the dictionnary of translations, to compress the given file
 * @param filePath The path of the uncompressed file
 */
void brynanumDictionnaryExtract(std::string filePath) {
	std::ofstream fileWrite(filePath); 
    for (HuffCodeMap::const_iterator it = tree.begin(); it != tree.end(); ++it)
    {
        fileWrite << it->first << FILE_SEPARATION;
        std::copy(it->second.begin(), it->second.end(),
                  std::ostream_iterator<bool>(fileWrite));
        fileWrite << std::endl;
    }
}

/**
 * @fn brynanumCompress
 * @brief Compress a file
 * @param fileStartPath The path of the uncompressed file
 * @param fileEndPath The path of the compressed file
 */
void brynanumCompress(std::string fileStartPath,std::string fileEndPath) {
	std::vector<bool> result;
	char c;

	std::ifstream fileStart(fileStartPath);
	while(fileStart.get(c)) {
		for(HuffCodeMap::const_iterator it = tree.begin(); it != tree.end(); ++it) {
			if(it->first==c) {
				result.insert(result.end(),(it->second).begin(),(it->second).end());			
			}
		}
	}

	std::ofstream fileEnd(fileEndPath);
	for(unsigned int i=0; i<result.size(); i++) {
		fileEnd << ((unsigned short int) result[i]);
	}
}

/**
 * @fn brynanumUncompress
 * @brief Uncompress a file
 * @param fileStartPath The path of the compressed file
 * @param fileEndPath The path of the uncompressed file
 */
void brynanumUncompress(std::string fileStartPath,std::string fileEndPath) {
	std::ifstream fileStart(fileStartPath);
	std::ofstream fileEnd(fileEndPath);	
	std::vector<bool> result;
	char c;
	while(fileStart.get(c)) {
		result.push_back(c=='1');
		for(HuffCodeMap::const_iterator it = tree.begin(); it != tree.end(); ++it) {
			if(it->second==result) {
				fileEnd << it->first;
				result.clear();
			}
		}
	}
}

/**
 * @brief  Main function
 * @param  argc An integer argument count of the command line arguments
 * @param  argv An argument vector of the command line arguments (action + path)
 * @return The condition of the exit program
 */
int main(int argc, char* argv[]) {
	if(argc!=3) {
		std::cout << "[program execution] <type_of_action> <path_of_the_file>\nFirst parameter : compress/uncompress/dictionnary\n\tcompress : compress your file as \".brynanum\"\n\tuncompress : uncompress a \".brynanum\", as \".txt\". Precise the path with \".txt\" instead of \".brynanum\".\n\tdictionnary : get the translation between \".txt\" and \".brynanum\" file, as \".dat\"\n\tall : Realize the three actions\nSecond parameter : path of a \".txt\" file.\n\nIf the file already exists, it is overwritten.\n";
		return -1;
	}
	std::string filePath=argv[2];
	tree=treeGenerate(filePath);
	filePath=filePath.substr(0,filePath.size()-4);
	
	switch(argv[1][0]) {
		case 'c':
			brynanumCompress(filePath+(std::string)".txt",filePath+(std::string)".brynanum");
		break;
		case 'u':
			brynanumUncompress(filePath+(std::string)".brynanum",filePath+(std::string)"New.txt");
		break;
		case 'd':
			brynanumDictionnaryExtract(filePath+(std::string)".dat");
		break;
		case 'a':
			brynanumDictionnaryExtract(filePath+(std::string)".dat");
			brynanumCompress(filePath+(std::string)".txt",filePath+(std::string)".brynanum");
			brynanumUncompress(filePath+(std::string)".brynanum",filePath+(std::string)"New.txt");
		break;
		default:
			std::cout << "The first parameter must be compress, uncompress, dictionnary or all.\n";
			return -1;
		break;
	}
}
